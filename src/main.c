/*
 * main.c
 *
 *  Created on: Apr 30, 2012
 *      Author: stanner
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct {
	int i;
	int j;
	int value;
}Node;

//function prototypes
bool oldNeighbor(Node I[], Node n);
bool hasNeighbor(Node P[], Node n);
bool skip(Node fail[], Node n, int f_count);
void print(int **A, int n, int moves);
void removePosition(int **A, int n, int i, int j);
void solve(int **A, int n);
void getPath(int **A, Node Q[], int *q_count, int n, Node I[], int i_count);
bool inQ(Node Q[], Node n);
void sortPath(Node Q[], int q_count);
void clearFail(Node fail[], int f_count);

int main (int argc, char *argv[]) {

	FILE *fp;
	int n;
	int **A;



	if (argc == 2 ) {
		fp = fopen(argv[1], "r");
		if (fp == NULL) {
			printf("Can't open %s\n", argv[1]);
			return 1;
		}

		//read in n
		fscanf(fp, "%d", &n);
		//printf("%d", n);

		//create the game board
		A = (int**) malloc (sizeof(int) * n);
		int i;
		for (i = 0; i < n; i++) {
			A[i] = (int *) malloc (sizeof(int) * n);
		}
		int j;
		int in;
		//Populate the game board from the input file
		for (i = 0; i < n; i++) {
			for (j = 0; j < n; j++) {
				fscanf(fp, "%d", &in);

				A[i][j] = in;
			}
		}

		//print(*&A, n);

		//removePosition(*&A, n, 3, 3);

		solve(*&A, n);




		//Game over
		return 0;
	}
	else {
		printf("please include only the input file when executing romensio_c");
		return 1;
	}
}

void print (int **A, int n, int moves) {
	//Print out the matrix
	int i;
	int j;
	int left = 0;
	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {

			if (A[i][j] == 0) {
				printf("  ");
			}
			else {
				printf("%d ", A[i][j]);
				left++;
			}

		}
		printf("\n");
	}

	printf("Numbers left: %d\n", left);
	printf("Number of moves: %d\n", moves);
}

void removePosition(int **A, int n, int i, int j) {

	int p;
	for (p = i; p > 0; p--) {
		A[p][j] = A[p - 1][j];
	}
	A[p][j] = 0;

}

void solve(int **A, int n) {
	Node L;
	Node Q[10];
	Node I[100];
	Node *fail = (Node *) malloc (sizeof(Node) * n * n);
	int f_count = 0;
	int moves = 0;

	int i;
	int j;

	Node clear;
	clear.i = 0;
	clear.j = 0;
	clear.value = 0;


	int a;
	for (a = 0; a < n*n; a++) {
		L.i = 0;
		L.j = 0;
		L.value = A[0][0];

		for (i = 0; i < n; i++) {
			for (j = 0; j < n; j++) {
				Node t;
				t.i = i;
				t.j = j;
				t.value = A[i][j];
				if (A[i][j] > L.value && !skip(fail, t, f_count)) {
					L.i = i;
					L.j = j;
					L.value = A[i][j];
				}
			}
		}
		if (L.value == 0) {
			break;
		}

		for (i = 0; i < 10; i++) {
			Q[i] = clear;
		}
		for (i = 0; i < 100; i++) {
			I[i] = clear;
		}
		int q_count = 0;
		int i_count = 0;
		Q[0] = L;
		q_count++;
		getPath(*&A, Q, &q_count, n, I, i_count);
		//So now Q Should either have a path or be null
		sortPath(Q, q_count);
		if (q_count > 1) {
			for (i = 0; i < q_count; i++) {
				removePosition(*&A, n, Q[i].i, Q[i].j);
				clearFail(fail, f_count);
			}
			moves++;
		}
		else {

			fail[f_count] = L;
			f_count++;
		}
		//print(*&A, n, moves);
			//Now We need to find a new L and start the recusive search again

	}

	print(*&A, n, moves);

}

void sortPath(Node Q[], int q_count) {
	int i;
	Node tmp;
	for (i = 0; i < q_count; i++) {
		int j;
		for (j = i; j < q_count; j++) {
			if(Q[i].i >= Q[j].i) {
				tmp = Q[i];
				Q[i] = Q[j];
				Q[j] = tmp;
			}
		}

	}
}
void clearFail(Node fail[], int f_count) {

	int i;
	Node clear;
	clear.value = 0;
	clear.i = 0;
	clear.j = 0;

	for (i = 0; i < f_count; i++) {
		fail[i] = clear;
	}
}

void getPath(int **A,Node Q[], int *q_count, int n, Node I[], int i_count) {


	//base case if q is empty
	if (*q_count == 0) {
		return;
	}

	int n_count = 0;

	Node P[100];
	int i;
	for (i = 0; i < 100; i++) {
		P[i].i = 0;
		P[i].j = 0;
		P[i].value = 0;
	}

	for (i = 0; i < *q_count; i++) {


		//NW
		if (Q[i].i - 1 >= 0 && Q[i].j - 1 >= 0 ) {

			Node NW;
			NW.i = Q[i].i - 1;
			NW.j = Q[i].j - 1;
			NW.value = A[NW.i][NW.j];
			if (!hasNeighbor(P, NW) && !oldNeighbor(I, NW) && NW.value && !inQ(Q, NW)) {
				P[n_count] = NW;
				n_count++;
			}
		}

		//N
		if (Q[i].i - 1 >= 0) {

			Node N;
			N.i = Q[i].i - 1;
			N.j = Q[i].j;
			N.value = A[N.i][N.j];
			if (!hasNeighbor(P, N) && !oldNeighbor(I, N) && N.value && !inQ(Q, N)) {
				P[n_count] = N;
				n_count++;
			}
		}

		//S
		if (Q[i].i + 1 < n) {
			Node S;
			S.i = Q[i].i + 1;
			S.j = Q[i].j;
			S.value = A[S.i][S.j];
			if (!hasNeighbor(P, S) && !oldNeighbor(I, S) && S.value && !inQ(Q, S)) {
				P[n_count] = S;
				n_count++;
			}
		}

		//NE
		if (Q[i].i - 1 >= 0 && Q[i].j + 1 < n) {

			Node NE;
			NE.i = Q[i].i - 1;
			NE.j = Q[i].j + 1;
			NE.value = A[NE.i][NE.j];
			if (!hasNeighbor(P, NE) && !oldNeighbor(I, NE) && NE.value && !inQ(Q, NE)) {
				P[n_count] = NE;
				n_count++;
			}
		}

		//W
		if (Q[i].j - 1 >= 0) {

			Node W;
			W.i = Q[i].i;
			W.j = Q[i].j - 1;
			W.value = A[W.i][W.j];
			if (!hasNeighbor(P, W) && !oldNeighbor(I, W) && W.value && !inQ(Q, W)) {
				P[n_count] = W;
				n_count++;
			}
		}
		//E
		if (Q[i].j + 1 < n) {

			Node E;
			E.i = Q[i].i;
			E.j = Q[i].j + 1;
			E.value = A[E.i][E.j];
			if (!hasNeighbor(P, E) && !oldNeighbor(I, E) && E.value && !inQ(Q, E)) {
				P[n_count] = E;
				n_count++;
			}
		}

		//SW
		if (Q[i].i + 1 < n && Q[i].j - 1 >= 0) {

			Node SW;
			SW.i = Q[i].i + 1;
			SW.j = Q[i].j - 1;
			SW.value = A[SW.i][SW.j];
			if (!hasNeighbor(P, SW) && !oldNeighbor(I, SW) && SW.value && !inQ(Q, SW)) {
				P[n_count] = SW;
				n_count++;
			}
		}

		//SE
		if (Q[i].i + 1 < n && Q[i].j + 1 < n) {

			Node SE;
			SE.i = Q[i].i + 1;
			SE.j = Q[i].j + 1;
			SE.value = A[SE.i][SE.j];
			if (!hasNeighbor(P, SE) && !oldNeighbor(I, SE) && SE.value && !inQ(Q, SE)) {
				P[n_count] = SE;
				n_count++;
			}
		}
	}
	//Now P is populated with all the Neighbors of the Nodes in Q

	Node T;
	T.value = 10;

	//Pick the smallest T
	for (i = 1; i < n_count; i++) {
		if (T.value > P[i].value && P[i].value != 0) {
			T = P[i];
		}
	}

	if (T.value == 10) {
		return;
	}

	//get the weight of Q
	int q_weight = 0;
	for (i = 0; i < *q_count; i++ ) {
		q_weight = q_weight + Q[i].value;

	}

	//Check the new weight
	int t_weight = q_weight + T.value;
	if (t_weight < 10) {
		Q[*q_count] = T;
		*q_count = *q_count + 1;
		return getPath(A, Q, q_count, n, I, i_count);
	}
	else if (t_weight > 10) {
		*q_count = *q_count - 1;
		Node last = Q[*q_count];
		I[i_count] = last;
		i_count++;
		Q[*q_count].i = 0;
		Q[*q_count].j = 0;
		Q[*q_count].value = 0;

		return getPath(A, Q, q_count, n, I, i_count);

	}
	else {
		Q[*q_count] = T;
		*q_count = *q_count + 1;
		return;
	}


}


//Check the set of neighbors for existing ones
bool hasNeighbor(Node P[], Node n) {
	int i;
	for (i = 0; i < 100; i++) {
		Node c = P[i];
		if (c.i == n.i && c.j == n.j && c.value == n.value) {
			return true;
		}
	}
	return false;
}

bool inQ(Node Q[], Node n) {
	int i;
	for (i = 0; i < 10; i++) {
		Node c = Q[i];
		if (c.i == n.i && c.j == n.j && c.value == n.value) {
			return true;
		}
	}
	return false;
}

bool skip(Node fail[], Node n, int f_count) {
	int i;
	if (!f_count) {
		return false;
	}
	for (i = 0; i < f_count; i++) {
		if (fail[i].value == n.value && fail[i].i == n.i && fail[i].j == n.j) {
			return true;
		}
	}
	return false;
}

//search the sed of old neighbors for exisiting ones.
bool oldNeighbor(Node I[], Node n) {
	int i;
	for (i = 0; i < 100; i++) {
		Node c = I[i];
		if (c.i == n.i && c.j == n.j && c.value == n.value) {
			return true;
		}
	}
	return false;
}
